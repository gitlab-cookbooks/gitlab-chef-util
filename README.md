# gitlab-chef-util Cookbook

Chef cookbook that holds basic helper objects and functions for general chef development

## Recipes

None, this project does not contain any recipe, and that's on purpose

## Requirements

- Chefspec

## License and Authors

License:

MIT

Authors:

- Pablo Carranza

