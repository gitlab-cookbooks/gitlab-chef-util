require "spec_helper"

describe "gitlab-chef-util::default" do
  cached(:chef_run) {
    ChefSpec::SoloRunner.new.converge(described_recipe)
  }

  it "Gets the private ip of a given node" do
    expect(GitLab::Net.private_ips_for_node(chef_run.node)).to eq(["10.0.0.2"])
  end
end

