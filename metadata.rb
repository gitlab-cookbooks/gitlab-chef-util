name             'gitlab-chef-util'
maintainer       'GitLab'
maintainer_email 'pablo@gitlab.com'
license          'MIT'
description      'Installs/Configures gitlab-chef-util'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
